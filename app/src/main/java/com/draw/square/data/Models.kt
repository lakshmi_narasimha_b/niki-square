package com.draw.square.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "squares")
data class Square(
    @ColumnInfo(name = "x") var xCoordinate: Float = 0f,
    @ColumnInfo(name = "y") var yCoordinate: Float = 0f,
    @ColumnInfo(name = "side") var side: Int = 0
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    override fun toString(): String {
        val x2Coordinate = xCoordinate + side
        val y2Coordinate = yCoordinate
        val x3Coordinate = xCoordinate + side
        val y3Coordinate = yCoordinate + side
        val x4Coordinate = xCoordinate
        val y4Coordinate = yCoordinate + side
        return "Square $id Coordinates:[" +
                "[$xCoordinate, $yCoordinate], " +
                "[$x2Coordinate, $y2Coordinate], " +
                "[$x3Coordinate, $y3Coordinate], " +
                "[$x4Coordinate, $y4Coordinate]] \n"
    }
}
