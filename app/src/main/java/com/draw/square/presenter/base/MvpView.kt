package com.draw.square.presenter.base

/**
 * Marker interface for MVP views in the system.
 * All view interfaces should derive from this one.
 */
interface MvpView
