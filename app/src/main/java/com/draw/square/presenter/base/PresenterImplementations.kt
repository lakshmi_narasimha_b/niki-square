package com.draw.square.presenter.base

import androidx.annotation.CallSuper

/**
 * Basic implementation which stores view in a property.
 */
abstract class PresenterImpl<V : MvpView> : MvpPresenter<V> {

    lateinit var view: V

    @CallSuper
    override fun onStart(view: V) {
        this.view = view
    }
}
