package com.draw.square.presenter

import com.draw.square.presenter.base.PresenterImpl
import com.draw.square.dagger.MainActivityContract
import com.draw.square.data.Square
import com.draw.square.database.SquaresDAO
import javax.inject.Inject

class MainActivityPresenter @Inject constructor(
    private val squaresDAO: SquaresDAO
) : PresenterImpl<MainActivityContract.View>(),
    MainActivityContract.Presenter {

    override fun onStart(view: MainActivityContract.View) {
        super.onStart(view)
        view.populateViews()
        drawSquares()
    }

    override fun onSaveSquareClicked() {
        view.getResetSeekbarProgress()
    }

    override fun saveSquareToDatabase(currentSquare: Square) {
        if (squaresDAO.insert(currentSquare).isNotEmpty()) {
            drawSquares()
        } else {
            view.showSavingFailedMessage()
        }
    }

    override fun onClearScreenSelected() {
        squaresDAO.delete()
        view.clearScreen()
    }

    private fun drawSquares() {
        val squaresList = squaresDAO.getAll()
        view.drawSquares(squaresList)
    }

    override fun onShowCorodinatesClicked() {
        view.showCoordinatesList(getListOfCoordinates())
    }

    private fun getListOfCoordinates(): List<String> {
        val coordinatesList: MutableList<String> = ArrayList()
        val squaresList = squaresDAO.getAll()
        for (square in squaresList) {
            coordinatesList.add(square.toString())
        }
        return coordinatesList
    }
}
