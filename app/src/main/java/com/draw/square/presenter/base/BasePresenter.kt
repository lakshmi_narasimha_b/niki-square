package com.draw.square.presenter.base

/**
 * Base interface for all MVP presenters.
 */
interface MvpPresenter<V : MvpView> {
    /**
     * Presenter's lifecycle launcher to be called once view is created and presenter is ready to start.
     */
    fun onStart(view: V)
}
