package com.draw.square.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.draw.square.data.Square

@Dao
interface SquaresDAO {
    @Query("SELECT * FROM squares")
    fun getAll(): List<Square>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg squares: Square): List<Long>

    @Query("DELETE FROM squares")
    fun delete()
}
