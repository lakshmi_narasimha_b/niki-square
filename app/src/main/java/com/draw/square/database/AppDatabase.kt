package com.draw.square.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.draw.square.data.Square

@Database(entities = [Square::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun squaresDao(): SquaresDAO

    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java, "squares"
                ) // allow queries on the main thread.
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
