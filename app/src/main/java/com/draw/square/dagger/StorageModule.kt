package com.draw.square.dagger

import android.content.Context
import com.draw.square.database.AppDatabase
import com.draw.square.database.SquaresDAO
import dagger.Module
import dagger.Provides

@Module
internal class StorageModule(private val context: Context) {
    // Has provides classes

    @Provides
    fun provideDatabase(): SquaresDAO =
        AppDatabase.getAppDatabase(context = context.applicationContext).squaresDao()
}
