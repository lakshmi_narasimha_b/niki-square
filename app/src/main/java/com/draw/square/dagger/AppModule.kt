package com.draw.square.dagger

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
internal class AppModule(private val application: Application) {
    @Provides
    @Singleton
    fun provideApplication(): Application {
        return application
    }
}
