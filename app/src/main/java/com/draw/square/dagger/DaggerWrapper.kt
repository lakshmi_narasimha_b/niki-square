package com.draw.square.dagger

import android.app.Activity

object DaggerWrapper {

    fun getMainActivityComponent(activity: Activity): MainActivityComponent {
        return DaggerMainActivityComponent.builder()
            .storageModule(StorageModule(activity.applicationContext))
            .build()
    }
}
