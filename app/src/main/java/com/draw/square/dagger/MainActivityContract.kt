package com.draw.square.dagger

import com.draw.square.presenter.base.MvpPresenter
import com.draw.square.presenter.base.MvpView
import com.draw.square.data.Square

interface MainActivityContract {

    interface View : MvpView {
        fun populateViews()
        fun getResetSeekbarProgress()
        fun clearScreen()
        fun drawSquares(squaresList: List<Square>)
        fun showSavingFailedMessage()
        fun showCoordinatesList(listOfCoordinates: List<String>)
    }

    interface Presenter : MvpPresenter<View> {
        fun onSaveSquareClicked()
        fun saveSquareToDatabase(currentSquare: Square)
        fun onClearScreenSelected()
        fun onShowCorodinatesClicked()
    }
}
