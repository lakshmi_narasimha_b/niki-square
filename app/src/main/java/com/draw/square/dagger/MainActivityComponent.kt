package com.draw.square.dagger

import com.draw.square.view.MainActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(
    modules = [AppModule::class, MainActivityModule::class, StorageModule::class]
)
interface MainActivityComponent {
    fun inject(activity: MainActivity)
}
