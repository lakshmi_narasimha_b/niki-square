package com.draw.square.dagger

import com.draw.square.presenter.MainActivityPresenter
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class MainActivityModule {

    @Binds
    @Singleton
    abstract fun provideMainPresneter(presenter: MainActivityPresenter): MainActivityContract.Presenter
}
