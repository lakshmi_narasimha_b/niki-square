package com.draw.square.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.draw.square.data.Square

// Non zero base progress to draw a square visible to the user.
const val BASE_PROGRESS = 2

/**
 * View which supports drawing multiple squares.
 */
@SuppressLint("ViewConstructor")
class SquareDrawingView(
    context: Context,
    attrs: AttributeSet?
) : View(context, attrs), SizeChangeListener {

    private lateinit var drawPaint: Paint
    private lateinit var squares: List<Square>
    private var plottedSquares: MutableSet<Int> = HashSet()
    private var screenWidthPx = 0
    // Initialise the coordinates with negative values.
    private var pointX1 = -1f
    private var pointY1 = -1f
    private var pointX2 = -1f
    private var pointY2 = -1f
    // Side of the square, based on the progress.
    private var delta = 0
    private var bitmapToDraw: Bitmap? = null
    // Canvas to save the previously drawn squares.
    private var canvasToSave: Canvas? = null
    // Initialise the progress with a non zero values to let the user know the click has created a
    // square
    private var currentProgress = BASE_PROGRESS

    private fun setupPaint() { // Setup paint with color and stroke styles
        drawPaint = Paint()
        drawPaint.apply {
            this.color = Color.BLACK
            this.isAntiAlias = true
            this.strokeWidth = 5f
            this.style = Paint.Style.STROKE
            this.strokeJoin = Paint.Join.ROUND
            this.strokeCap = Paint.Cap.ROUND
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        if (bitmapToDraw != null) {
            bitmapToDraw!!.recycle()
        }
        canvasToSave = Canvas()
        bitmapToDraw = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        canvasToSave!!.setBitmap(bitmapToDraw)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                pointX1 = event.x
                pointY1 = event.y
                onSquareSizeChanged(currentProgress)
            }
            else -> return false
        }
        return true
    }

    override fun onDraw(canvas: Canvas) {
        // Plot the already drawn squares , first.
        for (square in squares) {
            if (!plottedSquares.contains(square.id)) {
                canvasToSave?.drawRect(
                    square.xCoordinate,
                    square.yCoordinate,
                    square.xCoordinate + square.side,
                    square.yCoordinate + square.side,
                    drawPaint
                )
                plottedSquares.add(square.id)
            }
        }
        // Draw the  current square, produced on the user click action.
        if (pointX1 > 0 && pointY1 > 0 && pointX2 > 0 && pointY2 > 0) {
            canvas.drawRect(pointX1, pointY1, pointX2, pointY2, drawPaint)
        }
        canvas.drawBitmap(bitmapToDraw!!, 0f, 0f, null)
    }

    init {
        isFocusable = true
        isFocusableInTouchMode = true
        setupPaint()
        val displayMetrics = context.resources.displayMetrics
        screenWidthPx = displayMetrics.heightPixels
    }

    /**
     * Calculate the change in the size with respect to the screen size, so we can calculate the
     * diagonally opposite vertices coordinates
     */
    override fun onSquareSizeChanged(size: Int) {
        if (size > 0) {
            this.currentProgress = size
            delta = screenWidthPx / 100 * size
            pointX2 = pointX1 + delta
            pointY2 = pointY1 + delta
            postInvalidate()
        }
    }

    /**
     * Reset progress, when a square is saved, to start drawing a new square.
     */
    fun resetProgress() {
        currentProgress = BASE_PROGRESS
    }

    /**
     * Get the square corresponding to the last click made by the user.
     */
    fun getCurrentSquare(): Square {
        return Square(pointX1, pointY1, delta)
    }

    /**
     * Initialise the view with squares, saved locally , if any, to maintain the state.
     */
    fun setPreDrawnSquares(squares: List<Square>) {
        plottedSquares.clear()
        this.squares = squares
        postInvalidate()
    }
}

/**
 * Interface to propagate the size change from the slider to the SquareDrawingView.
 */
interface SizeChangeListener {
    fun onSquareSizeChanged(size: Int)
}
