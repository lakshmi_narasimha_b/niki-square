package com.draw.square.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SeekBar
import android.widget.Toast
import com.draw.square.dagger.MainActivityContract
import com.draw.square.R
import com.draw.square.dagger.DaggerWrapper
import com.draw.square.data.Square
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(),
    MainActivityContract.View {

    @Inject
    lateinit var presenter: MainActivityContract.Presenter
    private var listener: SizeChangeListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setListener()
        presenter.onStart(this)
    }

    override fun onResume() {
        super.onResume()
        setListener()
    }

    private fun setListener() {
        if (listener == null) {
            listener = this.view_square_container as SizeChangeListener
        }
    }

    private fun injectDependencies() {
        DaggerWrapper.getMainActivityComponent(this).inject(this)
    }

    override fun populateViews() {
        save_square.setOnClickListener { presenter.onSaveSquareClicked() }
        seekbar_square_size.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, progress: Int, p2: Boolean) {
                listener?.onSquareSizeChanged(progress)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                // Do noting
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                // Do noting
            }
        })
        show_coordinates.setOnClickListener { presenter.onShowCorodinatesClicked() }
    }

    override fun showCoordinatesList(listOfCoordinates: List<String>) {
        startActivity(
            CoordinatesListActivity.createIntent(
                this,
                listOfCoordinates.toString()
            )
        )
    }

    override fun drawSquares(squaresList: List<Square>) {
        view_square_container.setPreDrawnSquares(squaresList)
    }

    override fun showSavingFailedMessage() {
        Toast.makeText(this, "Saving failed ! Please Retry", Toast.LENGTH_SHORT).show()
    }

    override fun getResetSeekbarProgress() {
        seekbar_square_size.progress = 0
        view_square_container.resetProgress()
        saveCurrentSquare()
    }

    private fun saveCurrentSquare() {
        presenter.saveSquareToDatabase(view_square_container.getCurrentSquare())
    }

    override fun clearScreen() {
        view_square_container.postInvalidate()
    }

    override fun onStop() {
        super.onStop()
        listener = null
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_options, menu)
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.clearScreen -> {
                // Action goes here
                presenter.onClearScreenSelected()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
