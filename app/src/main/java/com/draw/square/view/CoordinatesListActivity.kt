package com.draw.square.view


import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.draw.square.R
import kotlinx.android.synthetic.main.activity_coordinates_list.*


class CoordinatesListActivity : AppCompatActivity() {

    companion object {
        private const val KEY_COORDINATES_LIST = "KEY_COORDINATES_LIST"

        fun createIntent(context: Context, coordinatesList: String): Intent {
            val intent = Intent(context, CoordinatesListActivity::class.java)
            intent.putExtra(KEY_COORDINATES_LIST, coordinatesList)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coordinates_list)
        val coordinatesList = intent.getStringExtra(KEY_COORDINATES_LIST)
        text_coordinates.text = coordinatesList
    }
}
